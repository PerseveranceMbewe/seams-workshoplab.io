---
fullname: Persverance Mbewe
goby: Percy
img: mbewe.jpg
links:
  -
    title: LinkedIn Profile
    url: https://www.linkedin.com/in/perseverance-nceba-mbewe-114b5393/
affiliation:
  -
    org:CSIR
    position: Junior Dev.
